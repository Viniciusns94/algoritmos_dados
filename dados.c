/* 
 * File:   dados.c
 * Autores: Vinicius Nogueira \Rodrigo Figueiredo
 *
 * Created on 21 de Outubro de 2014, 17:29
 * AEEEEEEEEEEEEEEEEEEEEEEEEE :)
 */
#include <stdio.h>
#include <stdlib.h>
//Atribuir os valores dos lados dos dados
void Preenche(int *dado1, int *dado2, int *dado3, int *dado4) {
    dado1[0] = 4;
    dado1[1] = 1;
    dado1[2] = 1;
    dado1[3] = 3;
    dado1[4] = 1;
    dado1[5] = 2;

    dado2[0] = 3;
    dado2[1] = 1;
    dado2[2] = 2;
    dado2[3] = 4;
    dado2[4] = 4;
    dado2[5] = 3;

    dado3[0] = 4;
    dado3[1] = 1;
    dado3[2] = 3;
    dado3[3] = 2;
    dado3[4] = 2;
    dado3[5] = 1;

    dado4[0] = 1;
    dado4[1] = 2;
    dado4[2] = 4;
    dado4[3] = 3;
    dado4[4] = 2;
    dado4[5] = 3;
}
void GiraDireita(int *dado) {
    int backup;
    backup = dado[4];
    dado[4] = dado[3];
    dado[3] = dado[2];
    dado[2] = dado[1];
    dado[1] = backup;
}
void GiraCima(int *dado) {
    int backup;
    backup = dado[4];
    dado[4] = dado[0];
    dado[0] = dado[2];
    dado[2] = dado[5];
    dado[5] = backup;
}
void GiraBaixo(int *dado) {
    int backup;
    backup = dado[2];
    dado[2] = dado[0];
    dado[0] = dado[4];
    dado[4] = dado[5];
    dado[5] = backup;
}
void GiroLateralES(int *dado) {
    int backup;
    backup = dado[0];
    dado[0] = dado[3];
    dado[3] = dado[5];
    dado[5] = dado[1];
    dado[1] = backup;
}
int matDado1[24][6], matDado2[24][6], matDado3[24][6], matDado4[24][6];

void Dados(int *dado, int num) {//Informa as coordenadas dos giros dos dados
    int i, j, contLinha = 0, matriz[24][6], giro = 0;
    while (contLinha < 24) {
        if (contLinha == 0) {
            for (i = 0; i < 6; i++) {
                matriz[contLinha][i] = dado[i];
            }
            contLinha++;
        }
        GiraDireita(dado);
        for (i = 0; i < 6; i++) {
            matriz[contLinha][i] = dado[i];
        }
        contLinha++;
        giro++;
        if (giro == 3 || giro == 6 || giro == 15) {
            GiraBaixo(dado);
            for (i = 0; i < 6; i++) {
                matriz[contLinha][i] = dado[i];
            }
            contLinha++;
        }
        if (giro == 9) {
            GiroLateralES(dado);
            for (i = 0; i < 6; i++) {
                matriz[contLinha][i] = dado[i];
            }
            contLinha++;
        }
        if (giro == 12) {
            GiraCima(dado);
            for (i = 0; i < 6; i++) {
                matriz[contLinha][i] = dado[i];
            }
            contLinha++;
        }
    }
    if (num == 1) {//Armazena os valores dos dados correspondentes ao numero
        for (i = 0; i < 24; i++) {
            for (j = 0; j < 6; j++) {
                matDado1[i][j] = matriz[i][j];
            }
        }
    }
    if (num == 2) {
        for (i = 0; i < 24; i++) {
            for (j = 0; j < 6; j++) {
                matDado2[i][j] = matriz[i][j];
            }
        }
    }
    if (num == 3) {
        for (i = 0; i < 24; i++) {
            for (j = 0; j < 6; j++) {
                matDado3[i][j] = matriz[i][j];
            }
        }
    }
    if (num == 4) {
        for (i = 0; i < 24; i++) {
            for (j = 0; j < 6; j++) {
                matDado4[i][j] = matriz[i][j];
            }
        }
    }
}
int main() {
    int i, j, k, l, num = 1, dado1[6], dado2[6], dado3[6], dado4[6];
    
    Preenche(dado1, dado2, dado3, dado4);
    Dados(dado1, num);
    num++;
    Dados(dado2, num);
    num++;
    Dados(dado3, num);
    num++;
    Dados(dado4, num);
    int m, diferente=0;
    for (i = 0; i < 24; i++) {
        for (j = 0; j < 24; j++) {
            for (k = 0; k < 24; k++) {
                for (l = 0; l < 24; l++) {//comparação para ver as posições diferentes
                    if (matDado1[i][0] != matDado2[j][0] && matDado1[i][0] != matDado3[k][0]
                            && matDado1[i][0] != matDado4[l][0] && matDado1[i][2] != matDado2[j][2]
                            && matDado1[i][2] != matDado3[k][2] && matDado1[i][2] != matDado4[l][2]
                            && matDado1[i][4] != matDado2[j][4] && matDado1[i][4] != matDado3[k][4]
                            && matDado1[i][4] != matDado4[l][4] && matDado1[i][5] != matDado2[j][5]
                            && matDado1[i][5] != matDado3[k][5] && matDado1[i][5] != matDado4[l][5]

                            && matDado2[j][0] != matDado3[k][0] && matDado2[j][0] != matDado4[l][0]
                            && matDado2[j][2] != matDado3[k][2] && matDado2[j][2] != matDado4[l][2]
                            && matDado2[j][4] != matDado3[k][4] && matDado2[j][4] != matDado4[l][4]
                            && matDado2[j][5] != matDado3[k][5] && matDado2[j][5] != matDado4[l][5]

                            && matDado3[k][0] != matDado4[l][0] && matDado3[k][0] != matDado4[l][0]
                            && matDado3[k][2] != matDado4[l][2] && matDado3[k][2] != matDado4[l][2]
                            && matDado3[k][4] != matDado4[l][4] && matDado3[k][4] != matDado4[l][4]
                            && matDado3[k][5] != matDado4[l][5] && matDado3[k][5] != matDado4[l][5]){
                        diferente++;
                        printf("\nDado 1: ");
                        for(m=0;m<6;m++){
                            if (m == 1 || m == 3) continue;
                            if (matDado1[i][m] == 1) printf(" Amarelo  ");
                            if (matDado1[i][m] == 2) printf(" Vermelho ");
                            if (matDado1[i][m] == 3) printf(" Verde    ");
                            if (matDado1[i][m] == 4) printf(" Azul     ");
                        }
                        printf("\n");
                        printf("Dado 2: ");
                        for(m=0;m<6;m++){
                            if (m == 1 || m == 3) continue;
                            if (matDado2[j][m] == 1) printf(" Amarelo  ");
                            if (matDado2[j][m] == 2) printf(" Vermelho ");
                            if (matDado2[j][m] == 3) printf(" Verde    ");
                            if (matDado2[j][m] == 4) printf(" Azul     ");
                        }
                        printf("\n");
                        printf("Dado 3: ");
                        for(m=0;m<6;m++){
                            if (m == 1 || m == 3) continue;
                            if (matDado3[k][m] == 1) printf(" Amarelo  ");
                            if (matDado3[k][m] == 2) printf(" Vermelho ");
                            if (matDado3[k][m] == 3) printf(" Verde    ");
                            if (matDado3[k][m] == 4) printf(" Azul     ");
                        }
                        printf("\n");
                        printf("Dado 4: ");
                        for(m=0;m<6;m++){
                            if (m == 1 || m == 3) continue;
                            if (matDado4[l][m] == 1) printf(" Amarelo  ");
                            if (matDado4[l][m] == 2) printf(" Vermelho ");
                            if (matDado4[l][m] == 3) printf(" Verde    ");
                            if (matDado4[l][m] == 4) printf(" Azul     ");
                        }
                        printf("\n");
                    }
                }
            }
        }
    }
    printf("\nNúmero de Possibilidades diferentes: %d\n", diferente);
    return (EXIT_SUCCESS);
}